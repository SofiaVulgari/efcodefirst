﻿using AutoMapper;
using EFCodeFirst.DTOs.MovieDTO;
using EFCodeFirst.Models;

namespace EFCodeFirst.Profiles
{
    public class MovieProfile : Profile
    {
        /// <summary>
        /// Profile for Movie DTOs
        /// </summary>
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.FranchiseId, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ForMember(cdto => cdto.Characters, opt => opt
                .MapFrom(c => c.Characters.Select(c => c.Id).ToArray()))
                .ReverseMap();

            CreateMap<Movie, MovieCreateDTO>()
                .ReverseMap();

            CreateMap<Movie, MovieUpdateDTO>()
                .ReverseMap();
        }
    }
}
