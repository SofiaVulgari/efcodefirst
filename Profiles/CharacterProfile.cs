﻿using AutoMapper;
using EFCodeFirst.DTOs.CharacterDTO;
using EFCodeFirst.Models;

namespace EFCodeFirst.Profiles
{
    /// <summary>
    /// Profile for Character DTOs
    /// </summary>
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()))
                .ReverseMap();

            CreateMap<CharacterCreateDTO, Character>()
                .ReverseMap();

            CreateMap<CharacterUpdateDTO, Character>()
                .ReverseMap();
        }
    }
}
