﻿using AutoMapper;
using EFCodeFirst.DTOs.FranchiseDTO;
using EFCodeFirst.Models;

namespace EFCodeFirst.Profiles
{
    /// <summary>
    /// Profile for Franchise DTOs
    /// </summary>
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(c => c.Id).ToArray()))
                .ReverseMap();

            CreateMap<FranchiseCreateDTO, Franchise>()
                .ReverseMap();

            CreateMap<FranchiseUpdateDTO, Franchise>()
                .ReverseMap();
        }
    }
}
