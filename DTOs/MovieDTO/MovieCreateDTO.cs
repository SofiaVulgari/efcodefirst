﻿using EFCodeFirst.Models;

namespace EFCodeFirst.DTOs.MovieDTO
{
    /// <summary>
    /// Movie DTO for creating
    /// </summary>
    public class MovieCreateDTO
    {
        public string? MovieTitle { get; set; }
        public string? Genre { get; set; }
        public int? ReleaseYear { get; set; }
        public string? Director { get; set; }
        public string? Picture { get; set; }
        public string? Trailer { get; set; }
    }
}
