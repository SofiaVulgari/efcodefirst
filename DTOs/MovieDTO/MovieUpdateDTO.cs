﻿namespace EFCodeFirst.DTOs.MovieDTO
{
    /// <summary>
    /// Movie DTO for Updating
    /// </summary>
    public class MovieUpdateDTO
    {
        public int Id { get; set; }
        public string? MovieTitle { get; set; }
        public string? Genre { get; set; }
        public int? ReleaseYear { get; set; }
        public string? Director { get; set; }
    }
}
