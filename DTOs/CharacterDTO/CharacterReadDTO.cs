﻿namespace EFCodeFirst.DTOs.CharacterDTO
{
    /// <summary>
    /// Character DTO for Reading
    /// </summary>
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        public string? FullName { get; set; }
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string? Picture { get; set; }
        public List<int>? Movies { get; set; }
    }
}