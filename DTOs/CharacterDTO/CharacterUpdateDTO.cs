﻿using EFCodeFirst.Models;


namespace EFCodeFirst.DTOs.CharacterDTO
{
    /// <summary>
    /// Character DTO for updating
    /// </summary>
    public class CharacterUpdateDTO
    {
        public int Id { get; set; }
        public string? FullName { get; set; }
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string? Picture { get; set; }
    }
}
