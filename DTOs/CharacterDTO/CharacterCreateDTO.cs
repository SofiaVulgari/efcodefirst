﻿using EFCodeFirst.Models;

namespace EFCodeFirst.DTOs.CharacterDTO
{
    /// <summary>
    /// Character DTO for creating
    /// </summary>
    public class CharacterCreateDTO
    {
        public string? FullName { get; set; }
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string? Picture { get; set; }
    }
}
