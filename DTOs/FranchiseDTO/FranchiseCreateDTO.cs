﻿using EFCodeFirst.Models;

namespace EFCodeFirst.DTOs.FranchiseDTO
{
    /// <summary>
    /// Franchise DTO for creating
    /// </summary>
    public class FranchiseCreateDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
    }
}
