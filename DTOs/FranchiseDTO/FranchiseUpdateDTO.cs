﻿using EFCodeFirst.Models;

namespace EFCodeFirst.DTOs.FranchiseDTO
{
    /// <summary>
    /// Franchise DTO for updating
    /// </summary>
    public class FranchiseUpdateDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
    }
}
