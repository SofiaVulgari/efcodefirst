﻿using EFCodeFirst.Models;

namespace EFCodeFirst.DTOs.FranchiseDTO
{
    /// <summary>
    /// Franchise DTO for reading
    /// </summary>
    public class FranchiseReadDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public List<int>? Movies { get; set; }
        //public List<Character>? Characters { get; set; }
    }
}
