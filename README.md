# Assignment 6 - Create a Web API and document it.

## Acknowledgments
This project was created as a solution for the 3rd assignment of the backend part of the Experis Academy (2022) with Noroff Education AS.

## Install
Clone the repository. The database diagram created from SSMS is included as a png file and is called `CodeFirstDatabaseDiagram.png`.
In the `appsettings.json` file on line 10 make sure to change the Data Source value with your own server name as shown bellow: 
```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "DefaultConnection": "Data Source=N-SE-01-9952\\SQLEXPRESS; Initial Catalog=CodeFirstAssignment6; Integrated Security=True; Encrypt = false;"
  }
}
```
Replace ``N-SE-01-9952\\SQLEXPRESS`` with your own Microsoft SQL Server name. Do not forget to add the additional backslash.
Run `update-database` in the NuGet Package Manager Console to create the database with the migrations.

## Usage
The purpose of this console application was to create an Entity Framework Code First workflow and an ASP.NET Core Web API in C#. 

## Dependencies
Visual Studio Community 22 (.NET 6),  Windows 10.
From NuGet the  following packages: 
- AutoMapper (11.0.1)
- AutoMapper.Extensions.Microsoft.DependencyInjection (11.0.0)
- Microsoft.EntityFrameworkCore.Design (6.0.8)
- Microsoft.EntityFrameworkCore.SqlServer (6.0.8)
- Microsoft.EntityFrameworkCore.Tools (6.0.8)
- Microsoft.VisualStudio.Web.CodeGeneration.Design (6.0.8)
- Swashbuckle.AspNetCore (6.4.0)

## Contributors
Sofia Vulgari - sofia.vulgari@se.experis.com <br>
Mattias Eriksson - mattias.eriksson@se.experis.com
