﻿using EFCodeFirst.Models;

namespace EFCodeFirst.Services
{
    /// <summary>
    /// Interface for the movie service
    /// </summary>
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetMovieByIdAsync(int id);
        public Movie GetSpecificMovie(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task UpdateMovieCharactersAsync(int MovieId, List<int> characters);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
