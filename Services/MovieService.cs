﻿using EFCodeFirst.Models;
using Microsoft.EntityFrameworkCore;

namespace EFCodeFirst.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieCharactersDbContext _context;

        public MovieService(MovieCharactersDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds a movie
        /// </summary>
        /// <param name="movie">Movie object</param>
        /// <returns>Returns the movie object</returns>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Checks if movie exists
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>Returns a bool</returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        /// <summary>
        /// Deletes a movie by Id
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns></returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets all the movies in the database
        /// </summary>
        /// <returns>Returns a list of all movies</returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(c => c.Characters)
                .ThenInclude(m => m.Movies)
                .ToListAsync();
        }

        /// <summary>
        /// Get a movie by id Task
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>Returns the movie</returns>
        public async Task<Movie> GetMovieByIdAsync(int id)
        {
            return _context.Movies
                .Include(c => c.Characters)
                .ThenInclude(m => m.Movies)
                .SingleOrDefault(f => f.Id == id);
        }

        /// <summary>
        /// Get a movie by id from the database
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>Returns the movie</returns>
        public Movie GetSpecificMovie(int id)
        {
            return _context.Movies
                .Include(c => c.Characters)
                .SingleOrDefault(f => f.Id == id);
        }

        /// <summary>
        /// Updates a movie
        /// </summary>
        /// <param name="movie">Movie object</param>
        /// <returns></returns>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates characters from movies
        /// </summary>
        /// <param name="movieId">Movie Id</param>
        /// <param name="characters">List of character</param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateMovieCharactersAsync(int movieId, List<int> characters)
        {
            Movie moviesToUpdate = await _context.Movies
                .Include(c => c.Characters)
                .ThenInclude(m => m.Movies)
                .Where(c => c.Id == movieId)
                .FirstAsync();

            List<Character> characterss = new();
            foreach (int charId in characters)
            {
                Character chara = await _context.Characters.FindAsync(charId);
                if (chara == null)
                    throw new KeyNotFoundException();
                characterss.Add(chara);
            }

            moviesToUpdate.Characters = characterss;
            await _context.SaveChangesAsync();
        }
    }
}
