﻿using EFCodeFirst.Models;
using Microsoft.EntityFrameworkCore;

namespace EFCodeFirst.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieCharactersDbContext _context;

        public FranchiseService(MovieCharactersDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds a franchise
        /// </summary>
        /// <param name="franchise">Franchise Object</param>
        /// <returns>Franchise object</returns>
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        /// <summary>
        /// Checks if franchise exists by Id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Returns bool</returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }

        /// <summary>
        /// Deletes a franchise By Id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns></returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Get all franchises 
        /// </summary>
        /// <returns>Returns a list of all franchises</returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises
                .Include(c => c.Movies)
                .ThenInclude(m => m.Characters)
                .ToListAsync();
        }

        /// <summary>
        /// Get a franchise by Id Task
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Returns the franchise</returns>
        public async Task<Franchise> GetFranchiseByIdAsync(int id)
        {
            return _context.Franchises
                .Include(c => c.Movies)
                .ThenInclude(m => m.Characters)
                .SingleOrDefault(f => f.Id == id);
        }

        /// <summary>
        /// Get a franchise by Id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Returns the franchise</returns>
        public Franchise GetSpecificFranchise(int id)
        {
            return _context.Franchises
                .Include(c => c.Movies)
                .SingleOrDefault(f => f.Id == id);
        }

        /// <summary>
        /// Updates a franchise
        /// </summary>
        /// <param name="franchise">Franchise object</param>
        /// <returns></returns>
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates movies in a franchise
        /// </summary>
        /// <param name="franchiseId">Franchise Id</param>
        /// <param name="movies">List of ints</param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateFranchiseMoviesAsync(int franchiseId, List<int> movies)
        {
            Franchise franchiseToUpdateMovies = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.Id == franchiseId)
                .FirstAsync();

            List<Movie> moviess = new();
            foreach (int movieId in movies)
            {
                Movie mov = await _context.Movies.FindAsync(movieId);
                if (mov == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                moviess.Add(mov);
            }

            franchiseToUpdateMovies.Movies = moviess;
            await _context.SaveChangesAsync();
        }
    }
}
