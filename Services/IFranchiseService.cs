﻿using EFCodeFirst.Models;

namespace EFCodeFirst.Services
{

    /// <summary>
    /// Interface for the franchise service
    /// </summary>
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetFranchiseByIdAsync(int id);
        public Franchise GetSpecificFranchise(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseMoviesAsync(int franchiseId, List<int> movies);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
    }
}
