﻿using System.ComponentModel.DataAnnotations;

namespace EFCodeFirst.Models
{
    /// <summary>
    /// Model for the franchise object
    /// </summary>
    public class Franchise
    {
        public int Id { get; set; }
        [MaxLength(150)]
        public string? Name { get; set; }
        public string? Description { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}
