﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace EFCodeFirst.Models
{

    public class MovieCharactersDbContext : DbContext
    {
        /// <summary>
        /// Tables of the database
        /// </summary>
        public DbSet<Character>? Characters { get; set; }
        public DbSet<Movie>? Movies { get; set; }
        public DbSet<Franchise>? Franchises { get; set; }

        public MovieCharactersDbContext([NotNullAttribute] DbContextOptions options) : base(options){}

        /// <summary>
        /// Seeding database with movies, characters and franchises.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Movies
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 1,
                    MovieTitle = "The Fellowship of the Ring",
                    Genre = "Fantasy/Adventure",
                    ReleaseYear = 2001,
                    Director = "Peter Jackson",
                    Picture = "https://hbomax-images.warnermediacdn.com/images/GXdu2ZAglVJuAuwEAADbA/tileburnedin?size=1280x720&partner=hbomaxcom&v=44a146b107d9a08f4ea585296a2e811a&host=art-gallery.api.hbo.com&language=sv-se&w=1280",
                    Trailer = "https://www.youtube.com/watch?v=xkiiEjz2O40",
                    FranchiseId = 1
                });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 2,
                    MovieTitle = "The Two Towers",
                    Genre = "Fantasy/Adventure",
                    ReleaseYear = 2002,
                    Director = "Peter Jackson",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/d/d0/Lord_of_the_Rings_-_The_Two_Towers_%282002%29.jpg",
                    Trailer = "https://www.youtube.com/watch?v=EWwNDEMD3wM",
                    FranchiseId = 1
                });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 3,
                    MovieTitle = "Harry Potter and the Philosopher's Stone",
                    Genre = "Fantasy/Adventure",
                    ReleaseYear = 2001,
                    Director = "Chris Columbus",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/7/7a/Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg",
                    Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo",
                    FranchiseId = 2
                });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 4,
                    MovieTitle = "Spider-Man: Far From Home",
                    Genre = "Fantasy/Adventure",
                    ReleaseYear = 2019,
                    Director = " Jon Watts",
                    Picture = "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/622B600020C84ED215AA7089FCC5043DE03F7F39E41579A1EA4EEB11C5C18CBE/scale?width=1200&aspectRatio=1.78&format=jpeg",
                    Trailer = "https://www.youtube.com/watch?v=iT-oWDWQtUk",
                    FranchiseId = 3
                });

            #endregion

            #region Characters

            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 1,
                    FullName = "Smeagol",
                    Alias = "Gollum",
                    Gender = "Male",
                    Picture = "https://static.wikia.nocookie.net/lotr/images/e/e1/Gollum_Render.png/revision/latest?cb=20141218075509"
                });

            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 2,
                    FullName = "Harry Potter",
                    Alias = "The boy who lived",
                    Gender = "Male",
                    Picture = "https://static.independent.co.uk/s3fs-public/thumbnails/image/2016/09/29/15/hp.jpg?quality=75&width=982&height=726&auto=webp"
                });

            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 3,
                    FullName = "Peter Parker",
                    Alias = "Spiderman",
                    Gender = "Male",
                    Picture = "https://i.ytimg.com/vi/BNcxTNrtRdk/maxresdefault.jpg"
                });

            #endregion

            #region Franchise

            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 1,
                    Name = "The Lord of the Rings",
                    Description = "Set in the fictional world of Middle-earth, the films follow the hobbit Frodo Baggins as he and the Fellowship embark on a quest to destroy the One Ring, to ensure the destruction of its maker, the Dark Lord Sauron."
                });

            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 2,
                    Name = "Harry Potter",
                    Description = "Harry begins his first year at Hogwarts School of Witchcraft and Wizardry and learns about magic. During the year, Harry and his friends Ron Weasley and Hermione Granger become entangled in the mystery of the Philosopher's Stone which is being kept within the school."
                });

            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 3,
                    Name = "Marvel Cinematic Universe",
                    Description = "The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios."
                });

            #endregion

            //many-to-many 
            modelBuilder.Entity<Character>()
                .HasMany(m => m.Movies)
                .WithMany(c => c.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    o => o.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    h => h.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    lt =>
                    {
                        lt.HasKey("CharacterId", "MovieId");
                        lt.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 3 },
                            new { CharacterId = 3, MovieId = 4 }
                        );
                    });
        }
    }
}
