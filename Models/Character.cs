﻿using System.ComponentModel.DataAnnotations;

namespace EFCodeFirst.Models
{
    //Model for the character object
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string? FullName { get; set; }
        [MaxLength(100)]
        public string? Alias { get; set; }
        [MaxLength(50)]
        public string? Gender { get; set; }
        public string? Picture { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}
