﻿using System.ComponentModel.DataAnnotations;

namespace EFCodeFirst.Models
{
    /// <summary>
    /// Model for the Movie object
    /// </summary>
    public class Movie
    {
        public int Id { get; set; }
        [MaxLength(150)]
        public string? MovieTitle { get; set; }
        [MaxLength(150)]
        public string? Genre { get; set; }
        public int? ReleaseYear { get; set; }
        [MaxLength(100)]
        public string? Director { get; set; }
        public string? Picture { get; set; }
        public string? Trailer { get; set; }
        public ICollection<Character>? Characters { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
    }
}
