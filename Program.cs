using EFCodeFirst.Models;
using EFCodeFirst.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
//Add AutoMapper
builder.Services.AddAutoMapper(typeof(Program));
//Configure Swagger
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Movie Database",
        Description = "An ASP.NET Core Web API for a datastore and interface to store and manipulate movie characters.",
        Contact = new OpenApiContact
        {
            Name = "Project Repository",
            Url = new Uri("https://gitlab.com/SofiaVulgari/efcodefirst")
        }
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

// Add our IFranchiseService and IMovieServise.
builder.Services.AddScoped(typeof(IFranchiseService), typeof(FranchiseService));
builder.Services.AddScoped(typeof(IMovieService), typeof(MovieService));
// Add our controllers
builder.Services.AddControllers().AddJsonOptions(x =>
 x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
//Add our dbContext
builder.Services.AddDbContext<MovieCharactersDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
