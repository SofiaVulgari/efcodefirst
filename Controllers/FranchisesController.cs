﻿using AutoMapper;
using EFCodeFirst.DTOs.CharacterDTO;
using EFCodeFirst.DTOs.FranchiseDTO;
using EFCodeFirst.DTOs.MovieDTO;
using EFCodeFirst.Models;
using EFCodeFirst.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFCodeFirst.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;
        private readonly IMovieService _movieService;

        public FranchisesController(MovieCharactersDbContext context, IMapper mapper, IFranchiseService franchiseService)
        {
            _context = context;
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        #region Generic CRUD

        /// <summary>
        /// Gets all franchises
        /// </summary>
        /// <returns>Returns list of franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises()
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises.Include(f => f.Movies).ToListAsync());
        }

        /// <summary>
        /// Gets a franchise by Id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Returns a franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchises.Include(m => m.Movies).Where(m => m.Id == id).FirstOrDefaultAsync();

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Updates franchise by Id
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /Todo
        ///     {
        ///        "id": 1,
        ///        "name": "The Lord of the Rings",
        ///        "description": "Set in the fictional world of Middle-earth."
        ///     }
        /// </remarks>
        /// <param name="id">Franchise Id</param>
        /// <param name="franchise">Franchise Object</param>
        /// <returns>Status Code 204 No Content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateFranchiseById(int id, FranchiseUpdateDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            Franchise franchiseModel = _franchiseService.GetSpecificFranchise(id);
            _mapper.Map(franchise, franchiseModel);
            _context.Entry(franchiseModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new franchise in the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /Todo
        ///     {
        ///        "name": "DC",
        ///        "description": "The world of DC heroes"
        ///     }
        /// </remarks>
        /// <param name="dtoFran">Franchise object</param>
        /// <returns>Created franchise</returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> CreateFranchise(FranchiseCreateDTO dtoFranchise)
        {
            if (_context.Franchises == null)
            {
                return Problem("Entity set 'MovieCharactersDbContext.Franchises'  is null.");
            }
            Franchise franchise = _mapper.Map<Franchise>(dtoFranchise);
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchiseById", new { id = franchise.Id }, _mapper.Map<FranchiseCreateDTO>(franchise));
        }

        /// <summary>
        /// Deletes a franchise by Id.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Status Code 204 No Content</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }

            var franchise = await _context.Franchises.Include(f => f.Movies).SingleOrDefaultAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        #endregion

        #region Reporting/Updating Related Data

        /// <summary>
        /// Gets all movies in a franchise by Id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Returns a list of all movies in the franchise
        /// </returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<List<MovieReadDTO>>> GetAllMoviesInAFranchise(int id)
        {
            var franchise = _franchiseService.GetFranchiseByIdAsync(id);
            List<Movie> movies = new();

            if (franchise == null)
            {
                return NotFound();
            }

            foreach (var item in franchise.Result.Movies)
            {
                movies.Add(item);
            }

            List<MovieReadDTO> movieDtos = new();
            foreach (Movie movie in movies)
            {
                movieDtos.Add(_mapper.Map<MovieReadDTO>(movie));
            }

            return Ok(movieDtos);
        }

        /// <summary>
        /// Gets all characters in a franchise
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Returns a list of all characters in the franchise. 
        ///If the same character is in two movies in the same franchise 
        ///they will appear two times, once for each movie.
        ///</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetAllCharactersInAFranchise(int id)
        {
            var franchise = _franchiseService.GetFranchiseByIdAsync(id);

            List<Movie> movies = new();

            if (franchise == null)
            {
                return NotFound();
            }

            List<CharacterReadDTO> charDtos = new();
            foreach (var item in franchise.Result.Movies)
            {
                movies.Add(item);

                foreach (var charItem in item.Characters)
                {
                    charDtos.Add(_mapper.Map<CharacterReadDTO>(charItem));
                }
            }

            List<MovieReadDTO> movieDtos = new();
            foreach (Movie movie in movies)
            {
                movieDtos.Add(_mapper.Map<MovieReadDTO>(movie));
            }

            return Ok(charDtos);
        }

        /// <summary>
        /// Updates a movie in a franchise by Id
        /// </summary>
        /// <remarks>
        /// For Franchise with Id 1 add the movie with Id 3 to it. 
        /// (Movies 1 and 2 need to be included -if you still want them in this franchise-, if not they will get deleted).
        /// Sample request:
        ///
        ///     PUT /Todo
        ///     [
        ///       1,
        ///       2,
        ///       3
        ///     ]
        /// </remarks>
        /// <param name="id">Franchise Id</param>
        /// <param name="movies">List of movies</param>
        /// <returns>Status Code 204 No Content</returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMovieInFranchise(int id, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateFranchiseMoviesAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movie.");
            }

            return NoContent();
        }

        #endregion

        /// <summary>
        /// Check if franchise exists by Id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Returns bool</returns>
        private bool FranchiseExists(int id)
        {
            return (_context.Franchises?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
