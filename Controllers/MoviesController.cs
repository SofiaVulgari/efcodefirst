﻿using AutoMapper;
using EFCodeFirst.DTOs.CharacterDTO;
using EFCodeFirst.DTOs.MovieDTO;
using EFCodeFirst.Models;
using EFCodeFirst.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFCodeFirst.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        public readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;
        public MoviesController(MovieCharactersDbContext context, IMapper mapper, IMovieService movieService)
        {
            _context = context;
            _mapper = mapper;
            _movieService = movieService;
        }

        #region Generic CRUD

        /// <summary>
        /// Gets all movies in the database
        /// </summary>
        /// <returns>Returns a list of all movies</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMovies()
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movies.Include(m => m.Franchise).Include(m => m.Characters).ToListAsync());
        }

        /// <summary>
        /// Gets a movie by Id
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>Returns a movie</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovieById(int id)
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }

            var movie = await _context.Movies.Include(m => m.Franchise).Include(m => m.Characters).Where(m => m.Id == id).FirstOrDefaultAsync();

            if (movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Creates a movie in the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///        "movieTitle": "The Batman",
        ///        "genre": "Fantasy/Action",
        ///        "releaseYear": 2022,
        ///        "director": "Matt Reeves"
        ///     }
        /// </remarks>
        /// <param name="dtoMovie">Movie Object</param>
        /// <returns>Returns created movie</returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> CreateMovie(MovieCreateDTO dtoMovie)
        {
            if (_context.Movies == null)
            {
                return Problem("Entity set 'MovieCharactersDbContext.Movies'  is null.");
            }

            Movie movie = _mapper.Map<Movie>(dtoMovie);
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovieById", new { id = movie.Id }, _mapper.Map<MovieReadDTO>(movie));
        }

        /// <summary>
        /// Updates a movie by Id
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /Todo
        ///     {
        ///        "id": 5,
        ///        "movieTitle": "The Batman",
        ///        "genre": "Action",
        ///        "releaseYear": 2022,
        ///        "director": "Matt Reeves"
        ///     }
        /// </remarks>
        /// <param name="id">Movie Id</param>
        /// <param name="movie">Movie Object</param>
        /// <returns>Status Code 204 No Content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMovieById(int id, MovieUpdateDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            Movie movieModel = _movieService.GetSpecificMovie(id);
            _mapper.Map(movie, movieModel);
            _context.Entry(movieModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a movie from the database by Id
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>Status Code 204 No Content</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        #endregion

        #region Reporting/Updating Related Data

        /// <summary>
        /// Updates a character in a movie by Id
        /// </summary>
        /// <remarks>
        /// For example, for Movie with Id 2 add the character with Id 2 to it. 
        /// (If there are other characters in the movie their id needs to be included in order to not get removed from the movie).
        /// Sample request:
        ///
        ///     PUT /Todo
        ///     [
        ///      1,
        ///      2
        ///     ]
        /// </remarks>
        /// <param name="id">Movie Id</param>
        /// <param name="characters">List of character</param>
        /// <returns>Status Code 204 No Content</returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateCharactersInMovie(int id, List<int> characters)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateMovieCharactersAsync(id, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid character.");
            }

            return NoContent();
        }

        /// <summary>
        /// Gets all characters in a movie by Id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>Returns a list of all characters in a movie</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetAllCharactersInAMovie(int id)
        {
            var movie = _movieService.GetMovieByIdAsync(id);
            List<Character> characters = new();

            if (movie == null)
            {
                return NotFound();
            }

            foreach (var item in movie.Result.Characters)
            {
                characters.Add(item);
            }

            List<CharacterReadDTO> charDtos = new();
            foreach (Character character in characters)
            {
                charDtos.Add(_mapper.Map<CharacterReadDTO>(character));
            }

            return Ok(charDtos);
        }

        #endregion

        /// <summary>
        /// Checks if movie exists by Id
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>Returns a bool</returns>
        private bool MovieExists(int id)
        {
            return (_context.Movies?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
