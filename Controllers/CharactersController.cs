﻿using AutoMapper;
using EFCodeFirst.DTOs.CharacterDTO;
using EFCodeFirst.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFCodeFirst.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;
        public CharactersController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region Generic CRUD

        /// <summary>
        /// Gets all characters from the database
        /// </summary>
        /// <returns>Returns a list of all characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharacters()
        {
            if (_context.Characters == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(await _context.Characters.Include(c => c.Movies).ToListAsync());
        }

        /// <summary>
        /// Get one character by id from the database
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <returns>Returns the character</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int id)
        {
            if (_context.Characters == null)
            {
                return NotFound();
            }

            var character = await _context.Characters.Include(m => m.Movies).Where(m => m.Id == id).FirstOrDefaultAsync();

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Updates character by Id
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /Todo
        ///     {
        ///        "id": 1,
        ///        "fullName": "Trahald",
        ///        "alias": "Gollum",
        ///        "gender": "Male",
        ///        "picture": "https://static.wikia.nocookie.net/lotr/images/e/e1/Gollum_Render.png/revision/latest?cb=20141218075509"
        ///     }
        /// </remarks>
        /// <param name="id">The Character Id</param>
        /// <param name="character">The Character object</param>
        /// <returns>Status Code 204 No Content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCharacterById(int id, CharacterUpdateDTO character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            Character GetSpecificCharacter(int id)
            {
                return _context.Characters
                    .Include(m => m.Movies)
                    .SingleOrDefault(f => f.Id == id);
            }

            Character characterModel = GetSpecificCharacter(id);
            _mapper.Map(character, characterModel);
            _context.Entry(characterModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new character
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /Todo
        ///     {
        ///        "fullName": "Tom Marvolo Riddle",
        ///        "alias": "Lord Voldemort",
        ///        "gender": "Male",
        ///        "picture": "https://en.wikipedia.org/wiki/Lord_Voldemort#/media/File:Lordvoldemort.jpg"
        ///     }
        /// </remarks>
        /// <param name="dtoChar">Character object</param>
        /// <returns>Created character</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> CreateCharacter(CharacterCreateDTO dtoChar)
        {
            if (_context.Characters == null)
            {
                return Problem("Entity set 'MovieCharactersDbContext.Characters'  is null.");
            }

            Character character = _mapper.Map<Character>(dtoChar);
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacterById", new { id = character.Id }, _mapper.Map<CharacterReadDTO>(character));
        }

        /// <summary>
        /// Deletes character by Id
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <returns>Status Code 204 No Content</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (_context.Characters == null)
            {
                return NotFound();
            }
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Checks if character exists by Id
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <returns>Returns Bool</returns>
        private bool CharacterExists(int id)
        {
            return (_context.Characters?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        #endregion
    }
}
